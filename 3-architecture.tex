\chapter{Architecture}
\label{chap:architecture}

\section{Introduction}
In this chapter, we will explain the architecture of this project, including the design phase and implementation details. First of all, in the overview we will present a global vision about the project architecture, identifying the visualisation server and other necessary modules. Secondly, we will focus on each module explaining its purpose in this project. Finally, we will present the pipeline in charge of data mining.


\section{Overview}
\label{sec:overview}
In this section we will present the global architecture of the project, defining the different subsystems that participates in the project. We can identify the following subsystems:

\begin{itemize}
\item \textbf{\textit{Visualisation system:}} This is the main part of the project, it will be able to process data and show it in different views.
\item \textbf{\textit{Search and indexing system:}} This system is composed of \textbf{\textit{Elasticsearch}}\cite{gormley_elasticsearch:_2014} a searching server in charge of providing necessary data to the visualisation server. Also is composed of a tool for real-time tweet collection called \textbf{\textit{Logstash}}\footnote{https://www.elastic.co/products/logstash}. Finally, in this system we can include \textbf{\textit{Twitter}}\footnote{http://twitter.com} which provides data to be analysed and represented.
\item \textbf{\textit{Orchestrator:}} We use \textbf{\textit{Luigi}}\cite{luigi} as orchestrator to build pipelines through our search and indexing system and the analytic services, in order to facilitate sentiment analysis.
\item \textbf{\textit{Analytic services:}} In this project we have used two analytic services. On one hand we have \textbf{\textit{Senpy}}\cite{corcuera_platas_development_2014} used to analyse sentiments and \textbf{\textit{Nerdy}}\cite{roman_gomez_development_2015} necessary to recognise entities in different tweets.

\end{itemize}

\begin{figure}[ht!]
	\centering
	\includegraphics[width=400pt]{img/Architecture.png}
	\caption{Architecture}
	\label{fig:architecture}
\end{figure}
In the following sections we are going to describe deeply subsystems involved in the project.

\section{Searching and indexing system}
Logstash service catches every data from Twitter and stores it in an Elasticsearch index. 
There are some configurable parameters:
\begin{itemize}
	\item \textbf{\textit{Input:}} Here we configure Twitter app keys and the keyword or hashtag($\#$) we want to get data from.
	\item \textbf{\textit{Output:}} In this parameter we configure where to store the data collected: The host, the index and the template used for data storage.
\end{itemize}

\section{Orchestrator}
Luigi is a Python module that helps you build complex pipelines of batch jobs\cite{luigi}. It handles dependency resolution, workflow management, visualization etc. 
This module needs a script describing the pipeline to follow.

\section{Analytic services}
Senpy is sentiment and emotion analysis server in Python developed by the Intelligent Systems Group, for this task we have used two plug-ins developed by Ignacio Corcuera\cite{corcuera_platas_development_2014}:
\begin{itemize}
	\item \textbf{\textit{sentiText plug-in:}} This one is used for sentiment analysis. It distinguish between positive, neutral or negative sentiment.
	\item \textbf{\textit{EmoTextANEW plug-in:}} This other one is used for emotion analysis. Emotions available are anger, disgust, fear, joy, neutral and sadness.
\end{itemize} 
NERDY is an acronym for Named Entity Recognition Server in Python, this analyser has been developed by Constantino Román\cite{roman_gomez_development_2015}. We have used it to get the entities that are mentioned in a tweet to make the analysis and filtering easier. The library used for this task was polyglot-es. 


We are going to describe below the pipeline created for data mining, that is the existing relations between subsystems.

\section{Pipeline}
Nowadays, Twitter has become an interesting scenario for social analysis. We can find in this micro-blogging site millions of interactions between users. As Twitter posts (tweets) are short and are being generated constantly, they are well-suited sources of streaming data for opinion mining and sentiment polarity detection. 
Also Twitter provides news about the person who is posting them: commentary on links, directed discussion, location information, status or any other content.
Moreover, Twitter does not distinguish between celebrities and other people, even if users belong to different ideology groups. Twitter has representation in many countries so it provides a global view about the topic.
This is why we have used Twitter as source of information. We are going to present the phases and technologies involved during data mining process below.
In figure \eqref{fig:pipeline} we can see the phases of the whole process.

\begin{figure}
	\centering
	\includegraphics[width=400pt]{img/pipeline}
	\caption{Pipeline Phases}
	\label{fig:pipeline}
\end{figure}

\subsection{Logstash}
This is the first phase of the pipeline. In this step, is necessary to define a strategy to follow. We need tweets related to football, so the strategy is to run Logstash during important match events. For this project we have chosen the FC Barcelona - Átletico de Madrid match, which was the quarter final of the UEFA Champions League. Logstash was configured to catch every tweet belonging to the hashtag ($\#$)FCBAtleti and store them in an Elasticsearch index.
\subsection{Elasticdump}
\label{chap:exportdata}
After the previous phase has been completed data is stored in an Elasticsearch index. Logstash stores many fields that are not necessary for this project so we are going to filter the index.
Fields required for this project are "user.location", "text", "id" and "@timestamp".

This task is done with elasticdump\cite{elasticsearchdump}, this tool allow us to export data from Elasticsearch to a JSON file.
The command used was: \begin{lstlisting}[frame=single]
elasticdump \
--input=http://localhost:9200/twitter_football \
--output=Data.json \
--searchBody '{fields:["text", "user.location", "@timestamp", "id"]}'
\end{lstlisting}
The result was a file called Data.json with only necessary fields.
\subsection{Sentiment and emotion analysis}
This phase has been automatized using the orchestrator, we have described tasks using a script called sefarad.py for sentiment analysis that has been developed by Enrique Conde.
Tasks described in this script are:
\textbf{\textit{FetchDataTask}},
\textbf{\textit{SenpyTask}},
\textbf{\textit{NerdyTask}} and
\textbf{\textit{ElasticsearchTask}}

We had to adapt this script to be able to also analyse emotions creating another one called sefarad-emo.py that has the same tasks as the described above. 
Now we are going to deeply describe each task.
\subsubsection{FetchDataTask}
\label{subsec:fetchdata}
The main goal of this task is to read the JSON file. This is the Python code describing this task:
\begin{lstlisting}[frame=single]
class FetchDataTask(luigi.Task):
"""
Generates a local file containing 5 elements of data in JSON format.
"""

#: the date parameter.
date = luigi.DateParameter(default=datetime.date.today())
field = str(random.randint(0,10000)) + datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")

def run(self):
"""
Writes data in JSON format into the task's output target.
The data objects have the following attributes:
* `_id` is the default Elasticsearch id field,
* `text`: the text,
* `date`: the day when the data was created.
"""
today = datetime.date.today()
with open('Data.json') as f: #Here enter the path to your JSON file 
j = json.load(f)
for i in j:
i["_id"] = i["id"]

with self.output().open('w') as output:
json.dump(j, output)
output.write('\n')

def output(self):
"""
Returns the target output for this task.
In this case, a successful execution of this task will create a file on the local filesystem.
:return: the target output for this task.
:rtype: object (:py:class:`luigi.target.Target`)
"""
return luigi.LocalTarget(path='/tmp/_docs-%s.json' % self.field)
\end{lstlisting}
\subsubsection{SenpyTask}
\label{subsec:senpytask}
This task loads data fetched with previous task and send it to Senpy tool in order to analyse 
data retrieved and check sentiments expressed. In this process we can see the main difference between both scripts: Sefarad.py makes requests to Senpy's sentiText plug-in and the other one to Senpy's EmoTextANEW plug-in.
Response is filtered and we get the sentiment or emotion and store it in a JSON file.
\subsubsection{NerdyTask}
This task loads data fetched with previous task and send it to Nerdy tool in order to analyse 
data retrieved and recognise names and entities.
This is the Python code describing this task:
\begin{lstlisting}[frame=single]
class NerdyTask(luigi.Task):

#: date task parameter (default = today)
date = luigi.DateParameter(default=datetime.date.today())
file = str(random.randint(0,10000)) + datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")

def requires(self):
"""
This task's dependencies:
* :py:class:`~.FetchDataTask`
:return: object (:py:class:`luigi.task.Task`)
"""
return FetchDataTask()

def output(self):
"""
Returns the target output for this task.
In this case, a successful execution of this task will create a file on the local filesystem.
:return: the target output for this task.
:rtype: object (:py:class:`luigi.target.Target`)
"""
return luigi.LocalTarget(path='/tmp/analyzed-%s.jsonld' % self.file)	

def run(self):
"""
Send data to Wrapper tool and retrieve it analyzed. Store data in a json file.
"""
with self.output().open('w') as output:
with self.input().open('r') as infile:
j = json.load(infile)
for i in j:
r = wrapper.service(u"%s" % i["text"], "polyglot-es")
i["_id"] = i["id"]
i["entities"] = r[0]
output.write(json.dumps(i))
output.write('\n')


\end{lstlisting}
\subsubsection{ElasticsearchTask}
\label{subsec:elastictask}
This task loads JSON data contained in the file produced in the previous step into an Elasticsearch index.
You set the index and doc-type data when you call function Luigi, we have used footballdata index for sentiment analysed tweets: 
\begin{lstlisting}[frame=single]
luigi --module sefarad Elasticsearch --index footballdata
--doc-type tweet
\end{lstlisting}
And we have used footballemotion index to store emotion analysed tweets:
\begin{lstlisting}[frame=single]
luigi --module sefarad-emo Elasticsearch --index footballemotion
--doc-type tweet
\end{lstlisting}

