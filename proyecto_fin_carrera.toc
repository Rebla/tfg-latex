\contentsline {chapter}{Resumen}{V}{chapter*.3}
\contentsline {chapter}{Abstract}{VII}{chapter*.5}
\contentsline {chapter}{Agradecimientos}{IX}{chapter*.7}
\contentsline {chapter}{Contents}{XI}{section*.8}
\contentsline {chapter}{List of Figures}{XV}{section*.10}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.12}
\contentsline {section}{\numberline {1.1}Context}{1}{section.13}
\contentsline {section}{\numberline {1.2}Project goals}{2}{section.14}
\contentsline {section}{\numberline {1.3}Structure of this document}{2}{section.15}
\contentsline {chapter}{\numberline {2}Enabling Technologies}{3}{chapter.16}
\contentsline {section}{\numberline {2.1}Introduction}{3}{section.17}
\contentsline {section}{\numberline {2.2}Polymer web components}{3}{section.18}
\contentsline {subsection}{\numberline {2.2.1}Element Catalog}{4}{subsection.20}
\contentsline {section}{\numberline {2.3}D3.js}{5}{section.22}
\contentsline {subsection}{\numberline {2.3.1}Selecting Elements}{6}{subsection.23}
\contentsline {subsection}{\numberline {2.3.2}Manipulating Data}{6}{subsection.24}
\contentsline {section}{\numberline {2.4}GeoJSON}{7}{section.25}
\contentsline {section}{\numberline {2.5}TopoJSON}{7}{section.27}
\contentsline {section}{\numberline {2.6}ElasticSearch}{7}{section.29}
\contentsline {subsection}{\numberline {2.6.1}Searching}{8}{subsection.30}
\contentsline {subsection}{\numberline {2.6.2}Adding data}{8}{subsection.39}
\contentsline {section}{\numberline {2.7}SPARQL}{9}{section.65}
\contentsline {chapter}{\numberline {3}Architecture}{11}{chapter.74}
\contentsline {section}{\numberline {3.1}Introduction}{11}{section.75}
\contentsline {section}{\numberline {3.2}Overview}{11}{section.76}
\contentsline {section}{\numberline {3.3}Searching and indexing system}{13}{section.80}
\contentsline {section}{\numberline {3.4}Orchestrator}{13}{section.81}
\contentsline {section}{\numberline {3.5}Analytic services}{13}{section.82}
\contentsline {section}{\numberline {3.6}Pipeline}{14}{section.83}
\contentsline {subsection}{\numberline {3.6.1}Logstash}{14}{subsection.85}
\contentsline {subsection}{\numberline {3.6.2}Elasticdump}{14}{subsection.86}
\contentsline {subsection}{\numberline {3.6.3}Sentiment and emotion analysis}{15}{subsection.92}
\contentsline {subsubsection}{\numberline {3.6.3.1}FetchDataTask}{15}{subsubsection.93}
\contentsline {subsubsection}{\numberline {3.6.3.2}SenpyTask}{16}{subsubsection.130}
\contentsline {subsubsection}{\numberline {3.6.3.3}NerdyTask}{16}{subsubsection.131}
\contentsline {subsubsection}{\numberline {3.6.3.4}ElasticsearchTask}{17}{subsubsection.171}
\contentsline {chapter}{\numberline {4}Visualisation Server}{19}{chapter.178}
\contentsline {section}{\numberline {4.1}Introduction}{19}{section.179}
\contentsline {section}{\numberline {4.2}Analysis}{19}{section.181}
\contentsline {subsection}{\numberline {4.2.1}Review of online visualisation dashboards for sentiment analysis}{20}{subsection.182}
\contentsline {subsection}{\numberline {4.2.2}Mock-up}{20}{subsection.186}
\contentsline {section}{\numberline {4.3}Widgets}{23}{section.191}
\contentsline {subsection}{\numberline {4.3.1}Player Ranking}{23}{subsection.192}
\contentsline {subsection}{\numberline {4.3.2}Team Ranking}{24}{subsection.194}
\contentsline {subsection}{\numberline {4.3.3}Sentiment Map}{24}{subsection.195}
\contentsline {subsection}{\numberline {4.3.4}Emotion Map}{25}{subsection.197}
\contentsline {subsection}{\numberline {4.3.5}Emotion Radar}{25}{subsection.198}
\contentsline {subsection}{\numberline {4.3.6}Tweet Chart}{25}{subsection.200}
\contentsline {subsection}{\numberline {4.3.7}Number Widget}{27}{subsection.202}
\contentsline {subsection}{\numberline {4.3.8}Sentiment Chart}{27}{subsection.203}
\contentsline {subsection}{\numberline {4.3.9}Top Team}{28}{subsection.205}
\contentsline {subsection}{\numberline {4.3.10}Top Player}{28}{subsection.207}
\contentsline {section}{\numberline {4.4}Dashboard Tabs}{29}{section.209}
\contentsline {subsection}{\numberline {4.4.1}Events tab}{29}{subsection.210}
\contentsline {subsection}{\numberline {4.4.2}Teams tab}{30}{subsection.211}
\contentsline {subsection}{\numberline {4.4.3}Players tab}{30}{subsection.212}
\contentsline {subsection}{\numberline {4.4.4}SPARQL editor tab}{31}{subsection.213}
\contentsline {chapter}{\numberline {5}Case study}{33}{chapter.215}
\contentsline {section}{\numberline {5.1}Introduction}{33}{section.216}
\contentsline {section}{\numberline {5.2}Collecting data}{33}{section.217}
\contentsline {section}{\numberline {5.3}Displaying data}{34}{section.218}
\contentsline {section}{\numberline {5.4}Conclusions}{39}{section.223}
\contentsline {chapter}{\numberline {6}Conclusions and future work}{41}{chapter.224}
\contentsline {section}{\numberline {6.1}Introduction}{41}{section.225}
\contentsline {section}{\numberline {6.2}Conclusions}{41}{section.226}
\contentsline {section}{\numberline {6.3}Achieved goals}{42}{section.227}
\contentsline {section}{\numberline {6.4}Problems faced}{42}{section.228}
\contentsline {section}{\numberline {6.5}Future work}{43}{section.229}
\contentsline {chapter}{Bibliography}{44}{section*.230}
